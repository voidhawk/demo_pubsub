import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ContentService } from './content/content.service';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { PublishComponent } from './content/publish/publish.component';
import { SubscribeComponent } from './content/subscribe/subscribe.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    PublishComponent,
    SubscribeComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ContentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
