import { Component, OnInit, Input } from '@angular/core';
import { ContentService } from '../content.service';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  @Input() receivedContent: string;

  private subscribeURL = "https://aeon-dev.atosresearch.eu:3000/subscribe/b1ea7809-c8fa-426f-b9a4-48fb90eb0b2d";
  private content: string = "MyContent";

  constructor(private contentService: ContentService) { 

    this.contentService.lastReceivedMsgStream$.subscribe(item => {this.setReceivedContent(item)})
  }

  ngOnInit() {

    this.contentService.subscribe4MsgContent(this.subscribeURL);
  }

  setReceivedContent(content: string) {

    this.receivedContent = content;
  }
}
