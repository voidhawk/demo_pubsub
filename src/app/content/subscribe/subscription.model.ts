export class AeonSubscription {
    public id?: string;
    public desc: string;
    public ip: string;
    public subkey?: string;
    public _id?: string;
 }
 