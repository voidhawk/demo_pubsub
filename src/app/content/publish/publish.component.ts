import { Component, OnInit } from '@angular/core';
import { ContentService } from '../content.service';

@Component({
  selector: 'app-publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.css']
})
export class PublishComponent implements OnInit {

  private publishURL = "https://aeon-dev.atosresearch.eu:3000/publish/c4f9f365-3cc6-4358-8742-c017a12cf311";
  private content: string;

  constructor(private contentService: ContentService) {

  }

  ngOnInit() {
  }

  onUpdateInput(event: Event) {

    this.content = (<HTMLInputElement>event.target).value;
    console.log("PublishComponent.onUpdateInput: " + this.content);
  }

  onSubmit() {

    //console.log("PublishComponent.onSubmit: " + this.content);
    this.contentService.publishMsgContent(this.publishURL, this.content);
  }
}
