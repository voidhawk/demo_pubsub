import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import { Subject } from 'rxjs/Subject';

import * as _ from 'lodash-es';

import { AeonSubscription } from "./subscribe/subscription.model";

declare var AeonSDK: any;

@Injectable()
export class ContentService {

    private lastReceivedMsg = new Subject<string>()
    lastReceivedMsgStream$ = this.lastReceivedMsg.asObservable();

    private extPublishSDK: any;

    constructor() {

    }

    publishMsgContent(url: string, msg: string) {

        console.log("ContentService.publishMsgContent(" + url + ", " + msg + ")");

        var that = this;
        var myTestContent = {
            "source": "pubsub",
            "msg": ""
        }
        myTestContent.msg = msg;

        var aeonSDK = new AeonSDK(url);
        aeonSDK.publish(myTestContent, function (result) {

            if (result.code === 200) {

                console.log("SUCCESS: publishMsgContent(..) " + JSON.stringify(myTestContent));
                console.log(JSON.stringify(result));
            } else {
                console.error('FAILURE: publishMsgContent(..)' + JSON.stringify(myTestContent));
            }
        });
    }

    subscribe4MsgContent(url: string) {

        console.log("subscribe4MsgContent(" + url + ") called: ");

        var subscription = new AeonSubscription();
        subscription.id = "myMail@atos.com";
        subscription.desc = _.now().toString();
        var sdk = new AeonSDK(url, subscription);
        var identifier = subscription.desc;
        console.log(identifier);

        var control = function control(controlMsg) {

            console.log("ContentService.subscribe4MsgContent(" + identifier + ") => Callback function control(): ", controlMsg);
            if (controlMsg.code == 250) { //you have been subscribed

                var persistantSubscription = sdk.getSubscription();
                console.log("SUBSCRIBED Identifier=" + identifier + ": " + JSON.stringify(persistantSubscription));
            }
            else {
                console.log("WATCH Identifier=" + identifier + ": " + JSON.stringify(persistantSubscription));
            }
        }

        var that = this;
        /**
         * 
         * @param msg This is the callback where I expect my notification of the AEON framework
         */
        var received = function received(msg) {

            console.log("Subscriber: " + sdk.getSubscription().desc + ", Content: " + JSON.stringify(msg));
            that.setLastContent(JSON.stringify(msg));
        }
        sdk.subscribe(received, control);
    }

    setLastContent(content: string) {

        this.lastReceivedMsg.next(content);
    }

}
